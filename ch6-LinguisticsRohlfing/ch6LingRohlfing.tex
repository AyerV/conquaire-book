\documentclass[a4,12pt,twosided,reqno,ngerman]{article}
%Fix the Umlaut rendering issue
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage[]{babel}
% URL
\usepackage{url}
\usepackage{hyperref}
\usepackage{natbib}
% GRAPHICS
\usepackage{graphics}
\usepackage[font=footnotesize,labelfont=bf]{subcaption}
% Wrapping text around figures
\usepackage{wrapfig}
\usepackage{listings}
% render PDF
\usepackage{pdfpages}
\usepackage{subfiles}
\usepackage{blindtext}
%\usepackage[pdftex,dvipsnames]{xcolor}  % Coloured text etc.
%---- Render graphics and diagrams in LaTeX with TikZ, not xcolor.
\usepackage{tikz}
\usepackage{tikzscale}
\usetikzlibrary{calc,backgrounds}
\graphicspath{} % Outsourcing Tikz and relative paths
\usepackage[caption=false]{subfig}
% use special characters and symbols
\usepackage{wasysym}
\usepackage{marvosym}
% MATH pkg
\usepackage{amsmath}
%\usepackage{mathtools}
%% The lineno packages adds line numbers. Start line numbering with
%% \begin{linenumbers}, end it with \end{linenumbers}. Or switch it on
%% for the whole article with \linenumbers after \end{frontmatter}.
\usepackage{lineno}
% Bash commands
\usepackage{listings}
\usepackage{xcolor}
% COLORED TITLE HEADERS AND SECTIONS
\usepackage{titlesec}
\usepackage{authblk} %Authors


\begin{document}
    
%make title bold and 14 pt font (Latex default is non-bold, 16 pt)
\title{\Large \bf Reproducibility of early understanding of verbs in children}

\author[1]{\bf Vidya Ayer}
\author[2]{Katharina Rohlfing}
\author[3]{Iris Nomikou}
\author[1]{Corresponding Author\thanks{vid@svaksha.com}}
\affil[1]{Faculty of Technology, Bielefeld University}
\affil[2]{Faculty of Psycholinguistics, Paderborn University}
\affil[3]{Department of Psychology, University of Portsmouth}

\date{29-May-2018}
\maketitle



%S-1
\section{ABSTRACT} \label{abstract}
%S-1

In this chapter we describe the collaborative work done with the \textbf{Psycholinguistics} research group at Paderborn University that studies linguistic development in infants. Their group understands how cognitive growth occurs in infants via language that increases linguistic growth and perception in children by using static object pairs with parents speaking the relevant verbs to the child during the experiment. Our computational reproducibility study found that cognitive development in 10-month-old children was stronger in linguistic development that was evident when we reproduced the linear regression calculations with the increase in looking times at the target as the dependent variable and infants’ age in days as an independent variable. As per the analysis, we conclude that the regression model did not attain significance, reconfirming that the change in performance was not linear for the experiment conducted on 9- and 10-month-old infants understanding of verbs.

\newcommand{\keywords}[1]{\textbf{\textit{Keywords:}} #1}
\begin{keywords}
Psycholinguistics, Language learning, Infants, Eye-tracking, Python, Pandas.
\end{keywords}


\section{INTRODUCTION} \label{intro}

The \textbf{Psycholinguistics} research group at Paderborn University is concerned with language development in young children. Its main research interest is how children acquire the meaning of words to reveal links between language and cognitive development and to analyze early meanings as “building blocks” for conceptual and linguistic thinking.

\subsection{Research questions} \label{researchQ}

There is a debate on the question whether nouns are acquired before verbs. In contrast to nouns – which can be easily singled out by holding an object or pointing to it – verbs are relational since they combine agents, their actions with some objects. In consequence, verbs appear more complex in their meaning structure. However, it is also possible that early use of nouns is cumulative as it also binds together situational elements. For example, an infant might say \textit{ball} but relate this noun to the action of rolling \cite{nelson1974concept}. 

In the study, the research group studied 9- and 10-month-old infants understanding of verbs using a technique similar to the \citeA{bergelson20126} ��study, except that verbs were used, in place of nouns. Following \citeA{mandler2012spatial}�� approach to concept development, we reasoned that infants must conceptualize situated actions early in their development to get concepts about objects off the ground. Early concepts, thus, will entail the role of objects, i.e., what the objects do and what is done to them \cite{mandler2012spatial}��, which provides a solid basis for the acquisition of verbs. Thus, the hypothesis was that children at a younger age, as found so far, will understand verbs that are drawn from their everyday life contexts. Instead of using dynamic pictures that refer to verbs, static object pairs were used and parent were asked to utter the relevant verbs. 

%SS-2.2
\subsection{Main result from the publication reproduced within Conquaire} \label{mainresult}
%SS-2.2
We matched the data analysis that found that the infants could demonstrate their verb understanding by looking significantly longer at the target word after it has been spoken by the parent. We further found a developmental difference, according to which 9-month-olds were not able to reliably demonstrate the verb understanding. With respect to early semantic development, as visualized by the target looking times, the data suggests that on hearing a verb the infants can associate it to object stimuli. This is in line with the researchers argument, according to which action concepts can be evoked in object perception. Furthermore, the results complement research proposing that children learn language by building relations and drawing from rich visual concepts \cite{nomikou2016language}��.



%S-3
\section{METHODS} \label{methods}
%S-3
Here, we describe the methods used for the experimental settings in the experiment.

%SS-3.1
\subsection{Experiment settings and Data acquisition pipeline} \label{XperimentData}
%SS-3.1
For the collaboration within the Conquaire project, they chose eye tracking data that was collected from 9- to 10-month old children in the course of a published study investigating early understanding of verbs. Infants were presented with paired-picture trials. The infants saw two images on the screen side by side, each one from a different context category (CARE or PLAY). They were shown for a total of 9.5s. Within the first 3s of each trial, parents heard a beep before they heard a sentence that they were asked to reproduce. Then, a second beep prompting them to begin repeating the sentence. While the parent was saying the target verb, the experimenter pressed a key on a wireless keyboard to mark the precise moment at which the verb was perceivable to the infant. This mark was logged into the data. An attention getter, i.e. a 3s clip featuring colorful animated shapes moving accompanied by different sounds, appeared after each trial. The experiment lasted 5 minutes. The entire visit lasted about 45 minutes.

Because of individual differences in the production of the target phrase by the parent, the post-target analysis window extended from 367 to 4.500 ms after the onset of the spoken target word. To calculate the onset of the target word, they used the recorded time-stamp of the keyboard key press and applied a Python script to split the looking times into two periods: before and after the uttered verb. Our dependent variable, namely, word comprehension, was thus operationalized by a difference score: the proportion of target looking upon hearing the target word (367 to 4.500 ms post keyboard keypress) minus the proportion of target looking before hearing the word (from when pictures were displayed until just before the keyboard keypress). This way, they obtained a difference score value that could be positive or negative. If the value was positive, it indicated infants’ increase of looking at the correct object after hearing the target verb, and thus their understanding of the target word. 


%SS-3.2
\subsection{Methods applied to analyze the data} \label{analyzedata}
%SS-3.2
The raw eye-tracking data were filtered using python scripts according to pre-defined areas of interest (AOIs). Then total gaze durations at the AOIs were calculated and subsequently the script took into account a specific timestamp generated by a key press of the keyboard and calculated the gaze durations before and after the keypress as well as the proportions of gaze at the target or distractor AOIs before and/or after the keypress. These calculations were formatted in a table and were used for further calculations. These involved before-after difference scores for each of the two presented instantiation's of each pair of stimuli, with the two difference scores being subsequently averaged. These difference scores were then used in a series of statistical tests: \textbf{t-tests}, \textbf{ANOVAs} and \textbf{binomial tests}.

Also in a subsequent review round of the submitted manuscript various versions of the initial script were created to repeat the analysis using a fixed time window for the inclusion/exclusion of data points. This was requested by the paper reviewers. To address this comment three new versions of the scripts were created with varying window durations, the changes incurred were assessed by comparing the results of a sample of data files and the usage of the script with a 4500ms time-window was selected to re-run the analysis and all the statistical tests.

During the creation and implementation of the scripts both initially and in the second round of analysis there was intensive interaction/collaboration between the research partners and the Conquaire team. This was necessary to check for errors in the scripts. For this, random manual calculations were performed on the raw data and then compared with the results produced by the scripts to test for accuracy. In some cases, multiple iterations were needed until the systematicity in the discrepancy between script and manually calculated results was discovered and corrected.


%SS-3.3
\subsection{Main Results} \label{mainresults}
%SS-3.3
After analyzing the keypress events timings recorded by the experimenter via the keyboard, the fixed time window was set at 4.500 ms after the keypress. The statistics conducted was a mixed, between, and within-subjects ANOVA1 with AGE (9 months vs. 10 months old) as the between-subjects variable, and TIME (before vs. after the word was spoken) as the within-subjects variable. Since a significant AGE x TIME interaction effect \begin{math} F(1, 46) = 5.687, p < .021, η2 = .107 \end{math} was found the data was treated in separate groups. Additionally, a linear regression was calculated with the increase in looking times at the target as the dependent variable and infants age in days as an independent variable. The regression model did not attain significance, suggesting that the change in performance was not linear, \begin{math} F(1,46) = 2.23, p = 0.142 \end{math}.

%S-4
\section{COMPUTATIONAL REPRODUCIBILITY} \label{ReX}
%S-4
Computational reproducibility experiments were conducted with the \textbf{Psycholinguistics} research group at Paderborn University at the paper publishing stage to fix the data analysis scripts and produce results, then implement visualizations with Pandas and matplotlib that was later stored in gitlab under continuous integration. 

%SS-4.1
\subsection{Research Data} \label{researchData}
%SS-4.1
To facilitate team-collaboration on porting and refactoring the code, the python scripts and extracted (TSV format) files for data analysis were procured from the researcher and uploaded to a private repository on our institutional gitlab installation. The research data is described below:

%sss-4.1.1 
\subsubsection{Primary Data} \label{primarydata}
%sss-4.1.1
The primary raw data (AVI files) videos are not available to the Conquaire researchers, hence not uploaded to the GitLab repo; which includes, the images seen by the infants on the screen, the parents voice recordings, the infants eye-tracking data and the 3s attention getter clip featuring colorful animated shapes moving to different sounds that appeared after each trial.

AVI (Audio Video Interleaved) files can contain both audio and video data in a file container that facilitates synchronous audio-with-video playback. 

%sss-4.1.2
\subsubsection{Analysis Data} \label{analysisdata}
%sss-4.1.2

The python scripts and extracted data (TSV) files for analysis are stored in the \textbf{data\_output} folder on gitlab. The research data structure (in the TSV and Excel) files are described below:

%\begin{verbatim}
The TSV files are stored in the "data\_output" folder within the subdirectory folders, viz. "tables\_3500", "tables\_4000" and "tables\_4500" for the three time windows. For example, to protect the identity and ensure the infant participants privacy, filenames are anonymized and named stored as "VP20\_output.tsv" etc..

In each file the various columns, like "Left\_before", "Left\_After", "Right\_Before", "Right\_After", "Fixation\_Direction", etc.., contain the measurements for each VP (Versuch Person). Within the same TSV document, starting from approx line 26, another header line contains a new set of measurements titled: Before, After, Bef\_Aft\_Tar, Target, Dis (before), Dis (after), Bef\_Atf\_Dis, T-D, T-D(B-A).
%\end{verbatim}

The sudden introduction of a header line in the middle of the Excel document makes it harder to automate the column-processing of all the data. Python libraries, like Pandas, when processing selected columns for applying statistical functions will fail when the \textbf{data-type} changes from the initial declaration. This means the programmer cannot automate the data file processing for standard statistics. Instead, they have to index the location of the specific variables that need to be parsed making it a costly abberation. 

%SS-4.2
\subsection{Data Workflow Lifecycle} \label{datLC}
%SS-4.2

The research data workflow lifecycle chart explains the sequence of the research data processing and tasks for this project:

%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics{images/fig1c4-workflow.pdf}
\caption{Data Workflow}
\label{fig-1:fig2-DataWorkflow-2}
\end{figure}
%\end{comment}
%%%%%%%%%%%%


%SS4.3
\subsection{Computational data formats and software tools} \label{CompDFSW}
%SS4.3

The research project used Free \& Open Source Software (FOSS) which increased the prospect of cross-platform availability of processing tools as Python programming language and visualisation packages (like Pandas, Matplotlib) are freely available for multiple platforms. 
 
The old data analysis scripts, written in Python version 2.x, were ported to version 3.6 for program maintenance due to end-of-life for Python version 2.x. Refactoring the old scripts from a complex mass of conditional loops, into a simplified modular callable program, was undertaken to ease program maintainance.

The main restructuring changes that were introduced are:
\begin{itemize}
  \item Most conditional loops were refactored into modular methods. Breaking the code apart into more logical components creates semantic units that are clear and reusable.
  \item A dict to store the vertical area of interest for each avi file.
  \item Introduced a class that acts as a wrapper for the dict (which stores the result of one avi file (aoi)) and other methods that can handle the logical componentization.
  \item A sliding time window to compensate for missing data points - this short time window allows searching for the next fixation data. Three time windows: 3.5ms, 4.0ms and 4.5ms (experimentLength = 3500/4000/4500)
\end{itemize}


\subsubsection{Data storage and file formats} \label{datastoreIO}

Two Excel sheets stored the analysis results \textbf{results\_simple\_difference\_score.xlsx} and \textbf{results\_simple\_target\_distractor.xlsx} while the analysis data is stored in tab-separated value (TSV) files. 

\textit{TSV} is a simple text format that widely supports data storage in a tabular structure that allows information exchange between databases or to transfer information from a database program to a spreadsheet and vice versa. The data record in the table is one line of the text file, with each value being separated by a tab character which acts as the general delimiter.

%SS-5.3
\subsection{Technical challenges and issues} \label{techBugs}
%ss-5.3

The results data files stored as Excel sheets \textbf{results\_simple\_difference\_score.xlsx} and \textbf{results\_simple\_target\_distractor.xlsx} were used for visualising the analysis results.

%sss-5.3.1
\subsubsection{File IO standards} \label{fileio}
%sss-5.3.1
Machines use standard formats to read in data and output it, and over the years computers have evolved various standards for processing data from files. Most file formats have \textbf{file headers} which can be termed as the \textit{internal metadata} that provides information about the data stored. This metadata is stored at the start of the file and is used by machines to quickly get information about a file without loading them into memory. Hence, the data files must be clean and ready for (pre-)processing data. 

Scientific computing uses a variety of file formats of which TSV, CSV and Excel workbook sheets continue to be the most commonly used formats by researchers with small amounts of research data. Some issues include:

\begin{itemize}
  \item Pandas expects file headers to be respected - Excel files with new set of column header starting in the middle of the file aids the researcher in visual data comparison but type changes during a column operation results in errors.
  \item Data IO and storage must use a non-proprietary and open format (e.g., CSV instead of Excel workbook sheets).
  \item Naming conventions for column headers must not include spaces or special characters.
  \item Pandas cannot process arbitrary changes in \textbf{dtypes} during column-oriented data processing.
  \item All columns must have header names.
\end{itemize}


\subsection{Computational reproducibility status} \label{ReXstatus}

Once the analysis script was ported to Python-3.6, it was possible to analyze the data and reproduce the results described in the paper as described in the section \ref{specificresult} above.  

A few graphs for specific results that were reproduced were generated as plots, as shown below: 

\subsubsection{Percentage difference in looking time (milliseconds), before and after for 8 verbs}

\newpage
\begin{figure}[!ht]
 \centering
 \includegraphics[width=9cm,height=15cm,keepaspectratio]{images/Graph-1.png}
 \caption{Fig.2. Percentage in milliseconds.}
 \label{fig: Percentage difference in looking time (milliseconds), before and after for 8 verbs}
 \protect
\end{figure}

\subsubsection{Looking times to target averaged over all the 8 verbs}

\begin{figure}[!ht]
  \centering
  \includegraphics[width=9cm,height=15cm,keepaspectratio]{images/Graph-2.png}
  \caption{Fig.3. Target looking time}
  \label{fig: Looking times averaged over 8 verbs}
   \protect
\end{figure}
\clearpage

Within the 9-month-old infant group, on average, the infants spent 51.1\% (SD = .056, MIN = 36.7\%, MAX = 63\%) of their looking time on the target object before the target word was spoken and 49.6\% (SD = .063, MIN = 35.4\%, MAX = 62\%) of their looking time on the target object after the word had been spoken. 

Within the 10-month-old infant group, on average, these infants spent 43.4\% (SD = .095, MIN = 25.9\%, MAX = 60.1\%) of their looking time on the target object before the target word was spoken and 49.6\% (SD = .081, MIN = 31\%, MAX = 64.1\%) of their looking time on the target object after the word had been spoken.


%S-5
\section{REVIEW AND RECOMMENDATIONS} \label{review}
%S-5
In this project we demonstrated how Free and Open Source Software (FOSS) can simplify the usage and access for a researcher interested in computational reproducibility of the data. Providing the data in gitlab enables easier access and also allows for multiple collaborators across the subject fields. 

In addition to the technical challenges and issues described in the \ref{techBugs} subsection, below, we list some of the open science recommendations regarding research data with respect to research reproducibility, in particular.

% ss-5.1
\subsection{Recommendations for Research Data FAIR-ness} \label{FAIRdat}
% ss-5.1

Some simple practices that encourage publishing open research and encourage scientists to practice open notebook science to publish and communicate scientific knowledge are described below: 


%SS-5.1.1
\subsubsection{Data should be Findable} \label{fairFind}
%SS-5.1.1
The group has tried to implement some open science principles by using FOSS tools, like Python, as the researchers were keenly interested in making their research "reproducibility friendly". 

\textbf{Metadata}: Due to the non-availability of access to the primary raw data sets, it is not possible to create a Linked Open Dataset (LOD) to semantically analyse this research data. The researchers must ensure that each data object is persistently identifiable via metadata and ontology tagging. 


%sss-5.1.2
\subsubsection{Data should be Accessible} \label{fairAccess}
%sss-5.1.2

The FAIR data principles require research data to be \textbf{Findable}, \textbf{Archivable}, \textbf{Interoperable} and \textbf{Reusable}.  

This requires:
\begin{itemize}
  \item The availability of machine-readable metadata that describes a digital resource for their research data. 
  \item A URL to a document that contains machine-readable metadata for the digital resource specifying the file format(s).
\end{itemize}
  
Currently, the non-existence of metadata as per FAIR\footnote{See the \url{< http://blog.ukdataservice.ac.uk/fair-data-assessment-tool/>}\href{ http://blog.ukdataservice.ac.uk/fair-data-assessment-tool/}{data}} data principles prevents the library from creating a semantically linked metadata resource for all the research data produced in the university. 


\subsubsection{Data should be Interoperable} \label{fairInter}

Being a RIFF (Resource Interchange File Format) file, AVI files can be tagged with metadata in the INFO chunk or embed as Extensible Metadata Platform (XMP). However, the raw data files for this research paper have not been provided, neither privately with a linkable URI, nor available elsewhere. 

%sss-5.1.4
\subsubsection{Data should be Reusable} \label{fairReuse}
%sss-5.1.4

\textbf{License}: 
As per the FAIR principles, research data must be released under an appropriate open data license while software code must be released under one of the Free software license schemes. Having the legal terms clearly stated upfront will reduce the ambiguity regarding reuse of data or code, and eliminate legal risks. 

Presently, the voice and video files (primary data) has not been released to the Conquaire project while the numerical (semi-processed) data are in *.tsv files. The anonymized research data could be stored privately on the gitlab instance. This would enable the researchers to provide private access to the data for collaborative data reuse.

\textbf{Data Privacy}: From the Open Science research reproducibility perspective, the criteria that \textit{scientific research, data and its artifacts} must be freely accessible and openly disseminated to everyone irrespective of their "researcher" status still requires to conform to the law. Clinical research data that deals with medical or patient information or personal children data or other personally identifiable information must conform to the European data privacy laws with informed consent. 

Data is "anonymized" when any information that can be used to identify a specific child participant has been erased or changed so that it cannot be used to identify the child directly. Some examples of personally identifiable information that could be removed in order to anonymize data would include: Real names, birth dates, gender, (partial) addresses, phone numbers, email addresses, etc..           

Raw data collected from the experiments on child development stages to measure linguistic development in infants must erase personally identifiable information and after anonymization it can be released as an open data set with the publication.


%S-6
\section{CONCLUSION}
%S-6
This paper has described a case study in computational reproducibility of results in the area of psycholinguistics. With the collaboration between the research team and the Conquaire research project, we were able to successfully delve into the reproducibility tools and the data analysis of child learning and cognitive dissonance, that sustained their research claim that cognition of verbs, although complex in structure, is a relatable action for an 8-10 month old child.

The research project would satisfy and successfully transition itself into an Open Research/ Open Science project if it were able to adopt some of the suggestions suggested in the Summary and Recommendations subsection \ref{summRecom} above.



%S-7
\section*{Acknowledgments}
%S-7
For their contributions to this chapter, we would like to thank:

\begin{itemize}
    \item \textbf{Lukas Biermann}, Conquaire student assistant, for the computational reproducibility attempt. 
    \item \textbf{Fabian Hermann}, Conquaire student assistant, for the gitlab continuous integration. 
\end{itemize}

\bibliographystyle{plain}
\bibliography{bib6ch}
\end{document}

