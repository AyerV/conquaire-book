\chapter{Introduction}

%S-1
\section*{ABSTRACT} \label{abstract}
%S-1

Data quality, FAIR-ness and computational reproducibility are achievable milestones in the field of academic research and research data management. They add credence to the open access movement by ensuring uniformity in data availability and reusability. Within the Conquaire project, we have strived to promote best practices by automating the data quality aspects to enable data reuse and computational reproducibility. Computational reproducibility experiments were conducted with each Conquaire partner, an attempt tp reproduce the results from their published papers inorder to obtain a closer perspective of their data and to review the state of data  FAIR-ness with respect to third-party reproducibility of the research analysis workflow. The Conquaire partners provided their paper to be reproduced along with the cleaned data sets used and the program scripts they used for the data analysis.


%SS-keywords
\subsubsection*{Keywords} \label{keywords}
Computational Reproducibility, Infrastructure System, Research Data Management, Case Studies, Git, Gitlab
%SS-keywords

%S-2
\section{INTRODUCTION} \label{intro}
%S-2

Reproducibility is a cornerstone of the scientific process. While the reproduction of an experiment can be extremely difficult, the ability to reproduce the (computational) analysis of the data that supported a certain conclusion (e.g. the validation of a hypothesis) should be a minimum requirement on every piece of published research. We call this type of reproducibility analytical reproducibility.

The ability to reproduce the analytic results of a certain piece of research requires, as a minimum, that: i) the primary or secondary data is available, ii) the data is syntactically well-formed and ready-to-use, iii) the data is appropriately documented, iv) the analysis procedures (e.g. scripts) that were used to process or analyze the data are available, and v) these analytic procedures can be run on the data to reproduce the actual result published in a paper. Analytical reproducibility is often hampered by the fact that one of the above requirements is not met.

An illustrative example of data sharing and independent validation or refutation of scientific results by others is conveyed by a recent case: In 2010 Harvard researchers Kenneth Rogoff and Carmen Reinhart published their paper “Growth in a Time of Debt” [18].  They had analyzed historical data from 20 industrialized countries since WWII and concluded that economic crises arise when the size of a country’s debt rises above 90\% of the Gross Domestic Product (GDP). This result had a significant impact on political decisions worldwide, until in 2013 the student Thomas Herndon tried to replicate the analysis.  He contacted Reinhart and Rogoff and they provided him with the actual working spreadsheet. Inspection of this spreadsheet disclosed several serious errors, which rendered the results invalid [6]. The above case is thus a very good example of the efficiency of the scientific process, albeit a rather simple one in which both the data and the analytical procedures used to analyze the data were available in the form of a spreadsheet and could thus be reproduced in a straightforward fashion. In many scientific disciplines, reproducing research results is more complicated as research can involve advanced and high-tech  devices needed to measure certain  phenomena, complex  experimental  protocols, data in different formats (structured vs. unstructured), different modalities (text, annotations, video, audio, 3D data) etc. which make it difficult to reproduce a certain scientific experiment.


While reproducing an experiment can be very challenging, as a baseline, given the primary or derived data resulting from an experiment, the reproduction of the (computational) analysis procedures that yielded a particular result should be feasible.  In fact, an important step in the generation of scientific results lies in the computational analysis of the primary data or derived secondary data. In most cases, software packages (such as SPSS, R, Excel) are used in this part of the process to test a hypothesis by performing some computational or statistical analyses of the primary or derived data. We will refer to this part of the process as analytical phase. While being able to fully reproduce an experiment can be extremely difficult, reproducing the analytical phase seems more feasible as it would essentially require access to the primary and/or derived data as well as to the analytical tools used by the researchers to derive some result.

Thus, a significant step towards supporting reproducibility in science would be \textbf{analytical reproducibility}, which consists of  making sure that a third party researcher could reproduce the computational and statistical analysis performed on primary and derived data to yield a particular conclusion; thus, being able to independently verify the results and conclusion. A crucial question is how research data infrastructures should be extended to support \textbf{analytical reproducibility}, \textbf{data sharing} and thus, \textbf{independent validation} of (analytical) research results. 

As a prerequisite for a research result or scientific paper to be analytically reproducible and useful for other researchers, the following conditions need to be met:

\begin{itemize}
  \item primary or secondary data is available for inspection and processing,
  \item data is syntactically well-formed and follows best practices from the corresponding community, thus being ready-to-use.
  \item analytic procedures (e. g. scripts, spreadsheets, etc.) that were used to process or analyse the data are available, and these analytic procedures can be run on the data to reproduce the actual result published in a paper.
\end{itemize}
     

Analytical reproducibility is often hampered by the fact that one of the above four elements is lacking. In order to be reproducible, research data needs to have a certain quality which we operationalize in the context of this proposal as its readiness to be re-used by others, e. g. to reproduce the computational analyses described in a scientific publication. Typically, researchers have no institutional support nor resources to ensure data quality in the above sense.  Thus, curating data to make it fit for publication and sharing requires substantial resources that researchers do not typically receive credit for. If the data is published, this is typically done in a delayed fashion after the research project or dissertation work  has  been  concluded. 

However, it is well-known from other areas (e.g. software  engineering) that quality control is better taken into account at the start of the research project. Continuous integration [2, p. 209] in software engineering is aimed at increasing quality of software by specifying a number of tests that the software should pass in order to continuously monitor compliance with these. Drawing inspiration from software engineering, principles of continuous integration could be applied to research data management to realize continuous monitoring of data quality and ensure that at each step in the research cycle, the data fulfills a number of defined tests.  Ultimately, as a final test on the quality of the research data, the proof that third parties can reproduce and validate the (computational) analyses that produced a certain result could be seen as final culmination of a continuous data quality assurance cycle.

In our previous research data management efforts [20, 10, 5, 22], we learned that researchers are generally willing to create data of high quality, share this data and make their results reproducible as part of their duties as a researcher and to meet expectations of their community. However, to ensure reproducible research extensive effort [\citealp{Buckheit1995}], maintenance and documentation is required which rarely happens due to the demanding challenges related to data processing, validation and publishing. Therefore, researchers need to be supported in this process by an appropriate institutional infrastructure that hosts their data and implements corresponding workflows that allow to ensure research data quality and reproducibility along the whole research lifecycle. Such an infrastructure that supports continuous research data quality monitoring and at the end makes the data publicly available to allow reproduction of the computational analysis is not yet available, and this proposal aims to close this gap.



%S-3
\section{Conquaire Quality Checking Service} \label{DatQty}
%S-3

For an academic institution, it is important to have an infrastructure based approach when creating research data management services for researchers. Within the Conquaire project, we have developed a number of best practices and workflows that support researchers in ensuring reproducibility and quality of their research data, concentrating on the analytical phase of an experiment. 

% fig1-ScientificWorkflow.png
%%%%%%%%%%%%
%\begin{wrapfigure}{r}{40mm}
\begin{figure}[!ht]  
  \centering
  \includegraphics[width=12cm,height=18cm,keepaspectratio]{./ch1-Introduction/images/fig1-ScientificWorkflow.png}
  \label{Fig.1: Scientific Workflow.}
  \caption{Fig.1: Scientific Workflow.}
\end{figure}
%\end{wrapfigure}
%%%%%%%%%%%%

Conquaire envisions that scientists commit their data and scripts early in the research cycle into a distributed version control system (DVCS) like Git, a content-addressable key-value data store based filesystem. A University-wide installation offers various advantages for collaboration, regular data backups that are version controlled, hence retrievable, and security features for data that cannot be corrupted.

The Conquaire project decided to adopt Git as the DVCS, largely to take advantage of features that ensure a distributed collaborative environment. Github, a social site for software development, used the Git DVCS as the underlying technology to create a cloud-hosted platform for sharing program code and related technical artefacts. With several collaborative features, the site was free for open-source projects and the intrinsic social features made it very popular among programmers, scientists and technical people wanting to share their work and collaborate. A Stackoverflow survey \footnote{\url{<https://insights.stackoverflow.com/survey/2015>}\href{https://insights.stackoverflow.com/survey/2015}{survey}} ranked git usage at 69.3\%, almost double than the second source control - SVN at 36.9\%, making git the front runner among distributed version control systems.

Since Github was a cloud-hosted platform, we looked for alternative FOSS implementations that could be installed on the University infrastructure. We found Gitlab, a libre software framework implementation of a web-based Git-repository manager that supports self-hosting with features similar to Github \footnote{\url{<https://conquaire.uni-bielefeld.de/2018/04/17/git/>}\href{https://conquaire.uni-bielefeld.de/2018/04/17/git/}{features similar to github}}, i.e. an issue-tracker, wiki, CI/CD pipeline, etc.. that was layered around the user with different permission levels. These variable permission layers for different feature access plays an important role in collaborating and sharing knowledge across physical boundaries. Like Github, the collaborative features of Gitlab include allowing a user to make multiple \textbf{commits}, \textbf{pull requests}, make changes and edit their documents, create forks or branches, revert to an old version, and/or merge those changes into the \textbf{master} branch. 

When a user makes a git commit, it consists of three steps that involves git creating, (i) a tree graph inorder to represent the content of the files being committed to the project, (ii) a commit object that is stored and tracked in the \textbf{.git/objects} folder, and (iii) an object that points the current branch at the new commit object.

To record the current state of the repository, git creates a tree graph from the index, which records the location and content of every file within the project repository. The tree graph is composed of two types of object: \textbf{blobs} and \textbf{trees}. The command \textit{git add} stores \textbf{blobs} that represent the content of files; while \textbf{trees} are stored when a \textit{commit} is made and it represents a directory in the working copy.

Thus, the distributed features of the key-value data store ensures that the git history stores the old version, the new version, and an interim version that the user stores in their forked (or, working copy) version. The git project environment aids data sharing and reproducibility when a user checks in research data into a version controlled repository, by ensuring they can reproduce the exact state of the project over a timeline. Thus, multiple users can easily collaborate without fear of their work being erased or overwritten thanks to many git features for collaboration like merging, fetching, pulling changes to a local branch, branching, stashing, pushing changes, tagging objects, etc...


%SSS-3.0.1
\subsubsection*{Gitlab's Open-Core Model} \label{opencore}
%SSS-3.0.1

Gitlab is a Ruby-based, Ruby-on-Rails (RoR) software that follows the \textbf{open-core} development model where the core functionality is released as \textbf{Gitlab CE (Community Edition)} under the MIT open source license while the \textbf{Gitlab EE (Enterprise Edition)} is under a commercial license, with a large open community behind it. 

While the \textit{open-core} development model allows companies to keep a basic version always available under an open source license, if in the future, Bielefeld University decides to buy the Enterprise Edition for implementation, it should keep in mind the instances of companies changing the licensing strategy suddenly. For eg., Redis Labs introduced a change to its module licensing strategy by adding the \textit{Commons Clause} to certain components (modules) of its open source software which prohibits large users, like cloud service providers, from freely installing it as a service without paying Redis for it. However, such clauses have the potential to inhibit the intangible benefits created by the community and destroy the open source model altogether. Companies looking to extract monetary value from their product, via the open core model, can be a potential risk for non-profit educational institutions, say Universities, that use them as part of their core infrastructure.


%SS-3.2
\subsection{Conquaire Workflow}  \label{CQRWF}
%SS-3.2

The workflow that we have designed within the Conquaire project is depicted below:

% fig2-ConquaireWorkflow.png
%%%%%%%%%%%%
%\begin{wrapfigure}{r}{40mm}
\begin{figure}[!ht]  
    \centering
    \includegraphics[width=12cm,height=18cm,keepaspectratio]{./ch1-Introduction/images/fig2-ConquaireWorkflow.png}
    \caption{Fig.2: Research data acquisition and processing pipeline.}
    \label{Fig.2: Research data acquisition and processing pipeline}
\end{figure}
%\end{wrapfigure}
%%%%%%%%%%%%

The architecture of the Conquaire quality control system is depicted in Fig. 2, where the workflow consists of two integrated streams: 

\paragraph{A. Data preparation and quality checking (marked red)}
\begin{itemize}
  \item \textbf{Step 1:} The researcher uploads data to the version control system server. This can be done by the GitLab Browser-based frontend, from the shell using Git commands or with any other available Git-Gui (e.g. Github Desktop, Tortoise Git)
  \item \textbf{Step 2:} Uploading one or more files onto the Git-Server automatically triggers the Gitlab CI-runner, which executes the quality checking procedures on the Conquaire quality checking server. These fetch the necessary files from the Git-repository and perform quality checks.
  \item \textbf{Step 3:} The result of the quality check is returned to the researcher. It gives a detailed analysis of all files that were checked and provides an overall grade for the data. The researcher may then correct the data according to the test results and resubmit it to the Git-repository. This cycle can be iterated as long as it is necessary.
\end{itemize}

\paragraph{B. Data publication und Re-Use (Steps 4-6, marked green)}
\begin{itemize}
  \item \textbf{Step 4:} If a researcher decides to publish the data, she can choose to do this on Bielefeld university’s publication repository PUB, which allows for data publication and has a direct interface to Gitlab. For publication of a dataset, the researcher only needs to enter the URL of the GitLab repository and some basic metadata (creator, year, license) into the PUB interface. 
  \item \textbf{Step 5:} PUB will automatically fetch the results of the Conquaire quality checks from the Conquaire server, and select and display the data quality badge.  
  \item \textbf{Step 6:} If a visitor wishes to download the data, it is fetched from GitLab. 
\end{itemize}

An important dimension of the workflow/architecture consists in automatic quality checks that can be applied to research data. Every time the researcher commits changes to the git repository, the in-built CI runner in Gitlab automatically executes a pipeline that is described in a yaml file inside the root directory of the project. This pipeline performs different tests that check whether all the necessary background information is provided (FAIR metrics[insert citation]) and the data has valid syntax and semantics. The quality checking tests are implemented in Python-3x and cover two open file types that are commonly used, i.e. \textbf{.csv} and \textbf{.xml}, with the possibility of an extension to work with other file types as well. 

In each case, the workflow is to search the repository for the specific file types and check if the files can be parsed in general and the data matches certain criteria. The researcher must provide \textbf{.fmt} or \textbf{.dtd} files with their own specifications (i.e., data value types and ranges) to evaluate the quality of their data. For every file and every commit into their git repository, a log file (bearing the commit hash) is created that lists errors and warnings that were reported by the test. 


% fig3-QualityFeedback.png
%%%%%%%%%%%%
%\begin{wrapfigure}{r}{40mm}
\begin{figure}[!ht]  
    \centering
    \includegraphics[width=6cm,height=6cm,keepaspectratio]{./ch1-Introduction/images/fig3-QualityFeedback.png}
    \caption{Fig.3: Example feedback from quality tests.}
    \label{Fig.3: Example feedback from quality tests.}
\end{figure}
%\end{wrapfigure}
%%%%%%%%%%%%


At the end, the feedback is provided directly to the researcher, helping them find errors and correct them right away before publishing the results. The feedback is stored on the server and an e-mail with an overall summary of the test results is sent to the researcher. In addition to the personal feedback, a colored badge icon in the PUB entry is created, representing the overall quality to other researchers who want to work with the data. The badge is based on the worst test result across the repository and has three possible categories: valid data (green), well-formed data (yellow), and erroneous data (red). This is to encourage researchers to make sure that only high-quality data is published to guarantee reproducibility.

We have applied the above mentioned workflow to a number of specific case studies from different disciplines in Conquaire. The case studies have been instrumental in validating the concept and workflow proposed by Conquaire but also in understanding the practical feasibility, obstacles etc. of the workflow when applied to concrete cases. In these case studies, we aimed for reproducing one particular result from an already published paper. This historical perspective might look a bit odd at first sight. 

However, we realized quickly that we would not been able to change the current workflows followed by researchers in the middle of a research project as this would be too disruptive, would delay their research process, a price that none of the groups involved in Conquaire, as case study partners, would be willing to pay. Therefore, with each of these case study partners, we identified one central result that Conquaire would try to reproduce. The specific desiderate were:

\begin{enumerate}
\item The data needed to reproduce the experiment should be deposited in Git.
\item The scripts needed to obtain the results should also be in Git.
\item Files and data formats should be documented.
\item If possible, data files should be under Conquaire, our automatized quality control system.
\end{enumerate}



%S-4
\section{COMPUTATIONAL REPRODUCIBILITY CASE STUDIES} \label{ReX}
%S-4

Here, we briefly describe the eight case studies analyzed in Conquaire; the main results that were reproduced from the published paper for each research group. The chapters contain a detailed report on the status of reproducibility as well as our recommendations based on FAIR principles.


%SS-4.0.1
\subsubsection*{Biological Cybernetics Research Group} \label{g1Duerr}
%SS-4.0.1
With the \textbf{Biological Cybernetics} research group at Bielefeld University that researches the adaptive locomotion abilities of stick and leaf insects, we reproduced the a published paper conducted on three insect species resulting in major differences related to antenna length, segment lengths of thorax and head, and the ratio of leg length over body length, with the long-legged Medauroidea having the strongest difference in intra-leg coordination of multiple joints, leg posture, and time courses of leg joint angles.


%SS-4.0.2
\subsubsection*{Neurobiology Research Group} \label{g2Egelhaaf}
%SS-4.0.2
The \textbf{Neurobiology Research Group} worked on the research topic of flight positions of bumblebee where Lobecke et al. [\citealp{Lobecke2018}] reported that the first learning flights of bumblebees are highly variable and depend on the recorded individual. The toolbox was designed concisely around a CI pipeline to use it to run the steps required to build the codebase in the right order of execution. The 3D trajectory generated by using triangulation from two 2D trajectories method was matched by the toolbox with the build where automated unit tests are run and deployed in a local machine successfully. 

%SS-4.0.3
\subsubsection*{Chemistry Research Group} \label{g3Koop}
%SS-4.0.3
The \textbf{Chemistry} Research group publication looked at the ice nucleation 



%SS-4.0.4
\subsubsection*{Psycholinguistics Research Group} \label{g4Rohlfing}
%SS-4.0.4
In our collaboration with the \textbf{Psycholinguistics} research group at Paderborn University, we looked at reproducing their findings of 10-month-old children demonstrating verb understanding by looking significantly longer at the target word after it was spoken by the parent which also signalled a developmental difference in 9-month-olds who were not able to reliably demonstrate the verb understanding. Furthermore, our work on the computational reproducibility aspect, we first refactored the Python code that was being used for the data analysis by the research group into classes and methods and also ported it from 2.7 to ver.3.6, then used Jupyter notebooks for their data visualisation needs.


%SS-4.0.5
\subsubsection*{DSG Research Group} \label{g5Schlangen}
%SS-4.0.5
First draft written. No response from DSG group, so plan to write full chapter myself.


%SS-4.0.6
\subsubsection*{Neuro-Cognitive Psychology Research Group} \label{g6Schneider}
%SS-4.0.6
With the \textbf{neuro-cognitive psychology} research group at Bielefeld University, we reproduced the results for their published manuscript entitled \textbf{Expectation violations in sensorimotor sequences: shifting from Long-Term Memory (LTM)-based attentional selection to visual search}; where we were able to reproduce the analysis results for expectation violations in a well-learned sensorimotor sequence in humans, causing a regression from LTM-based attentional selection to visual search using the proprietary SPSS tool for data analysis.


%SS-4.0.7
\subsubsection*{Economic Theory And Computational Economics (ETACE) Research Group} \label{g6vdHoog}
%SS-4.0.7
The Economic Theory And Computational Economics (ETACE) group applies agent modelling approaches to study dynamic equilibrium models resulting from the interaction of heterogeneous rational agents; allowing insights into the application of different industrial policy measures in different regions, the existence of varying spatial frictions on goods and labour markets, the spatial dynamics of industrial activity, technical change and growth, the micro- and macro-prudential regulations and their effects on micro-fragility and macro-financial stability, and financialisation of the real sector and the need for productive credit for economic development. We implemented the FLAViz library that implements a data analytic processing pipeline allowing the computational analysis and visualization of data simulation data generated in the FLAME environment. This library is a key step towards ensuring computational reproducibility of the analyses of the available simulation data.


%SS-4.0.8
\subsubsection*{Central-Labs Research Group} \label{g8Wachsmuth}
%SS-4.0.8
Central-Labs is a robotics research group that studies human-robot interaction (HRI). Their group placed a high premium on the experimental reproducibility of any result in HRI studies and heavily relies on the availability of all the research artefacts. Due to a higher rate of tool-chain automation in the robotic experiment stage, they were able to get a scientist with a non-technical background to reproduce a complex robotic experiment, a full replication, with minor differences in computational reproducibility results.


%sss-4.1
\subsection{Levels of Reproducibility} \label{ReXlevels}
%sss-4.1

Within Conquaire, we defined a taxonomy to distinguish the different levels of computational reproducibility based on the FAIR data principles. 

\begin{outline}[enumerate]
\begin{enumerate}
\item \textbf{Sustainable analytical reproducibility}
  \begin{enumerate}[label*=\arabic*.]
  \item Stability in environment (hardware and software) ensures full analytical reproducibility over a long term period.
  \item Ongoing regular maintenance of research artefacts, including maintenance of the workflow pipeline dependency
    \begin{enumerate}[label*=\arabic*.]
    \item Availability of raw data and processed data.
    \item Open toolkits for computations with dependency maintenance.
    \item Usage of open data sets, plus experiment data artefacts (computational scripts) being available openly.
    \end{enumerate}
  \end{enumerate}
\item \textbf{Full analytical reproducibility:} 
  \begin{enumerate}[label*=\arabic*.] 
  \item Data and scripts are available for reproduction; 
  \item The research project is well-documented; 
  \item The pipeline relies on open data formats and open tools.
  \end{enumerate}
\item \textbf{Limited analytical reproducibility:} 
  \begin{enumerate}[label*=\arabic*.] 
    \item Data is available, that includes software analysis scripts, raw data and intermediate processed data and results are available;
    \item Toolkit lock-in: Original results can be obtained, but either data is in non-standard formats or pipeline relies on commercial tools that cannot be easily inter-changed or replaced by open software.
    \item No documentation is available.
    \item Open or non-open data set.
  \end{enumerate}
\item \textbf{Non-reproducible:} 
  \begin{enumerate}[label*=\arabic*.] 
  \item Software versioning and library dependency issues, viz.
    \begin{enumerate}[label*=\arabic*.]
    \item Non-maintenance of the original library used in the research project,
    \item Library dependency version mismatch due to feature changes, etc. 
    \end{enumerate}
  \item Original results cannot be obtained in spite of data and scripts being available
    \begin{enumerate}[label*=\arabic*.]
    \item Some raw data input is live data and cannot be reproduced (e.g. voice recordings used as input, live video recording analysis, etc.),
    \item And/or, because reproduction requires manual intervention, e.g.changing the order of script execution, unknown script parameters, etc..
    \end{enumerate}
  \end{enumerate}
\item \textbf{Publication Only:} 
  \begin{enumerate}[label*=\arabic*.]
  \item No data or analysis code is available, neither publicly nor elsewhere.
  \item Data (software, raw and intermediate) cannot be shared at all due to privacy, commercial or IP restrictions. 
     \begin{enumerate}[label*=\arabic*.]
     \item Medical data of severe diseases, or, rare genetic(ally inherited) diseases where the patient(s) identity can be compromised or revealed unintentionally. Eg. the recent case of world's first gene-edited babies created in China.
    \end{enumerate}
  \item Data transfer restrictions due to government laws on sensitive government data, security related data, etc. 
  \end{enumerate}
\end{enumerate}
%\end{enumerate}
\end{outline}


%sss-4.2
\subsection{Data formats and Data sizes} \label{dataformats}
%sss-4.2

Our observation of the data formats and sizes shows, three groups used proprietary software formats and the rest used open software. One research group required high-performance computing (HPC) facilities, unavailable at Bielefeld University, to generate large amounts of simulated data with a visualization pipeline to process their data, while the rest of the groups had a long tail of research data with a multitude of formats used for images, video and audio data. However, research groups that had raw data in the form of large videos, images and audio cannot use gitlab to store the raw data due to its file size limit of 2GB per file with the GitLFS extension. The most common data format was CSV (and .tsv), Matlab (.m, .mat), Python (.py), XML, YAML (.yml), and Jupyter (.ipynb) notebooks. 


%sss-4.3
\subsection{Tools} \label{tools}
%sss-4.3

Most research groups had a combination of technical tools used for processing and analysing data. The most common programming language was Python (Theano, Pandas, Numpy, Scipy, scikit/sklearn, jupyter, etc.), closely followed by Matlab, for analysis and visualisation. The technical stack also included other tools like MySQL, C, Java and their libraries for processing tasks. The proprietary toolkits had Origin and SPSS that handled the data processing spectrum, from analysis to visualisation.


%sss-4.4
\subsection{Publication of data and scripts} \label{OpenPub}
%sss-4.4

Based on the FAIR principles, we defined a taxonomy for grading the status of the entire data set, (viz. raw data, processed and analysed data and the analysis scripts), its openness (open availability and accessibility), the licenses used, etc. As discussed above, raw data in the form of large videos, images and audio cannot be stored on gitlab due to the 2GB file size limit, hence some researchers used services like Sciebo or private machines to store the large data. Where open data sets were used, the researchers automated the process of downloading the data for cleaning and analysing it.

%sss-4.5
\subsection{Reproducibility Analysis} \label{ReXanalysis}
%sss-4.5

In Conquaire, out of the 8 groups that participated in the computational reproducibility experiments, we developed a visualisation tool for one group, analysed the computational processing toolkit (developed as a library) for a second case study, computationally reproduced five papers using opensource (and alternatively mixed) software, with one research paper being reproducible only with the original proprietary software. 

Furthermore, an evaluation on the basis of FAIR \textit{facets} data principles shows that \textit{none} of the current research groups satisfied the \textbf{full analytical reproducibility} taxonomy criterion that ensured stability in the data collection and processing pipeline to ensure full analytical reproducibility over a long-term period.

\textit{Five} case studies satisfied the \textbf{limited analytical reproducibility} taxonomy criterion by providing analysis data and scripts that were well-documented and used an analysis pipeline that relied on open data formats and open tools for the computation process; making it easier for them to achieve fully sustainable analytical reproducibility in future. They fell short of full reproducibility, either due to proprietary toolkit lock-ins for data in non-standard formats or non-availability of analysis code (also a data artefact) for reuse.

\textbf{Two} case studies were \textbf{non-reproducible} due to HPC constraints and data availability although their analysis pipelines used open source software, were publicly available and used open source licenses. We developed a visualisation tool for one group and analysed the computational processing toolkit (developed as a python library) for a second case study

And, finally, we discovered \textbf{one} case study was \textbf{publication only} due to the non-availability of data (software, raw and intermediate) artifacts as per FAIR principles, creating a dependency cycle. Here, only the binary executables of the program scripts were publicly available to enable users to reproduce the original results which falls short of the FAIR data principle of \textbf{reuse} and \textbf{interoperability}; thereby creating a dependency cycle on the original research group to provide updates and fixes to their code as the original components, due to its non-availability, cannot be easily inter-changed or replaced by alternative open software as the original code and documentation of the architecture is missing. This demonstrates that while the analysis pipeline uses free and open source software, the products created from it may not necessarily be openly available, nor reproducible.

To summarize, the taxonomy labels are an important grading tool that helps us define, grade and create benchmarks for research data based on industry standards like FAIR data principles. 

%S-6
\section{LESSONS LEARNED} \label{Lessons}
%S-6

In toto, the reproducibility experiments conducted with our partners was a learning experience on various levels, viz., the \textit{research toolkit workflow diversity} and the \textit{subject-specific differences} that threw-up various challenges to be overcome. In part this is a common trait in academia, where writing software is not the core task of a researcher, rather, it a part of the toolkit that they use while conducting research that leads them to achieving the paper publications and conference presentation goals. 

Researchers mostly write single-use program code that reaches the end of its life after publication i.e. the data and computational results are meant for a single publication, hence not much effort is made to invest in designing long-term use software. However, reproducibility is intricately tied to reusing software and modularizing the core components of their research is a step towards reuse and workflow toolkit sharing.

Within Conquaire, inorder to create a unified generic infrastructure for research data, it is imperative to encompass the diversity in technology, viz. the platform tools used, various file formats, contrasting workflow methodologies, non-uniform storage strategies, statistical outcome visualisation, etc.. This \textit{rainbow-hued diversity} created a challenging, time consuming and highly resource-intensive task of finding homogeneity within a heterogeneous environment. A few research groups were already  It also underlines the importance of unified foundational platform for storing and tracking the implementation and impact of open research data management infrastructure system.

On the reproducibility spectrum, the researchers ranged from \textit{not being very optimistic about their results being reproducible} to some researchers \textit{planning about long-term sustainability} of their work. The latter was still a challenge due to the ever-changing landscape of technology and software development viz. libraries getting (a) new versions, (b) dropped features, (c) core devs abandoning the project, (d) dependency hell, (e) no support or community, etc.. In the case of proprietary or commercial software it is harder to sustain reproducibility without the involvement of the original researchers. 

We posit that the term \textit{data} encompasses not only raw and semi-processed data but also the software used in the data analysis process. The FAIR data principles requires data to be \textbf{interoperable} and \textbf{reusable} while using community agreed formats, language and vocabularies that maintain the initial richness of data. Data must have a clear machine readable licence and provenance information on how the discipline-specific data was formed with metadata providing contextual information that allows data reuse. Thus, for data to be reusable, it is important to have the source scripts that allow the data to be modified to satisfy reusability criterion that depends on sustaining software version dependency standards.

Licensing is another important aspect of fulfilling the FAIR principles of publishing papers as open access and sharing data openly, the lack of which reduces the open impact factor significantly. Most times researchers are not equipped to deal with the plethora of licenses that are different for software (eg. free software and open-source licenses), paper publishing (eg. creative commons), and data (eg.ODPL). They may not know what licence to choose from this wide range for different research artefacts but releasing their work without any license renders it non-reusable (hence also non-interoperable) as per the FAIR principles. New versions of licenses are regularly released and the intricate and complex nuances of these changes makes it a complicated decision making process without advice on the effects of the legalese involved. 

The pharmaceutical, medical and other sensitive (government, security, etc..) research fields may keep the data \textit{private} either to protect the personal data of the research participants, protect government national interests and/or wish to retain the option of commercializing their intellectual property rights. 

All these aspects of research introduce challenges and new paradigms for researchers and their research groups who have to overcome the technical challenges and cope with the ever-changing technology world on one hand and the legal aspects of their research outputs on the other. These mundane administrative aspects of the research world can be confusing to navigate without a support system to provide timely and relevant advice and guidance on aspects not related to their core research work and organisations must be aware of them.

It can be claimed that the open science and open access movements are, in part, fuelled by the funder mandate; however, researchers have always been motivated to validate their research work via the peer-review process. For researchers who are more tech-savvy, social media offers tangible benefits and incentives like citation, badges, tweets and links that can be measured for the impact factor. Early adopters riding the technology wave have co-opted and led the path to a more open world within science.

%S-7
\section{CONCLUSION} \label{conclusion}
%S-7

\bibliographystyle{plain}
\bibliography{bib1ch}
%\end{document}
