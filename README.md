# README - The Conquaire Book 

URL: https://gitlab.ub.uni-bielefeld.de/svaksha/conquaire-book/

The git repository for the Conquaire Book on computational reproducibility of published research papers of eight project partners. 
Each case study partner has contribute sub-sections in the respective reproducibility chapter based on their published research paper.

This repository has the following folders where each partner can submit a chapter as per the LaTeX template inside the respective group folders (chapterNumber-Faculty+GroupHead), with images and bib references: 

* __../01-Frontmatter/..__:: The folder contains tex files for the Title (`titlepage.tex`), Copyright (`copyright.tex`), Dedication (`dedication.tex`), Preface (`preface.tex`) which includes acknowledgments and thanks. 
* __../02-Backmatter/..__:: Backmatter (`index.tex`) for the book.
* __../ch1-Introduction/..__:: Chapter1- Introduction to Conquaire, eight reproducibility case studies from interdisciplinary research fields.
* __../ch2-BiologyDuerr/..__:: Chapter2- Biology-Dürr group, Reproducibility of active tactile sensing abilities in insects.
* __../ch3-BiologyEgelhaaf/..__:: Chapter-3: Biology Faculty, Egelhaaf group.
* __../ch4-ChemistryKoop/..__:: Chapter-4: Faculty of Chemistry - Koop group.
* __../ch5-EconomicsHoog/..__:: Chapter-5: Faculty of Economics - van der Hoog group.
* __../ch6-LinguisticsRohlfing/..__:: Chapter-6: Faculty of Linguistics
* __../ch7-LinguisticsSchlangen/..__:: Chapter-7: Faculty of Linguistics, Schlangen group
* __../ch8-PsychologySchneider/..__:: Chapter-8: Faculty of Psychology, Schneider group
* __../ch9-TechnologyWachsmuth/..__:: Chapter-9: Faculty of Technology, Wachsmuth group


## Building Latex
1. Fork the repo to your namespace.
2. Write and store your TeX file in your group folder. Please store figures in the `images` sub-folder within your group folder with the bibliography references file.
3. Check if all the TeX package dependencies are installed.
4. Then, to build a pdf book locally, run the `MainBook.tex` file with the command: `xelatex MainBook.tex` or `pdflatex MainBook.tex`, from the top level directory. 
* Commit your changes/files into git and push to __your__ fork on gitlab (remote repository), or, directly to the Conquaire namespace if you have commit rights.
* Please file an issue if you have any trouble. 

Thanks!

----

# LICENSE 
COPYRIGHT © 2017-Now, Vidya AYER. This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License - (CC BY-NC-SA 4.0) as detailed in the `LICENSE.md` file and all copies and forks of this work must retain the Copyright, Licence and this permission notice.

