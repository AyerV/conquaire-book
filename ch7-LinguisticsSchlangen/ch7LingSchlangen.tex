\documentclass[a4,12pt,twosided,reqno]{article}
%Fix the Umlaut rendering issue
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage[]{babel}
% URL
\usepackage{url}
\usepackage[obeyspaces]{url}
\usepackage{hyperref}
%Cite-BIB
\usepackage{natbib}
%\usepackage{bibtopic}
%\usepackage[options, square,sort,comma,numbers]{natbib}
% Math pkg
\usepackage{siunitx}
% GRAPHICS
\usepackage{graphics}
\usepackage[font=footnotesize,labelfont=bf]{subcaption}
% Wrapping text around figures
\usepackage{wrapfig}
\usepackage{listings}
\usepackage{pdfpages}
%\usepackage[pdftex,dvipsnames]{xcolor}  % Coloured text etc.
%---- Render graphics and diagrams in LaTeX with TikZ, not xcolor.
\usepackage{tikz}
\usepackage{tikzscale}
\usetikzlibrary{calc,backgrounds}
\graphicspath{} % Outsourcing Tikz and relative paths
\usepackage[caption=false]{subfig}
% use special characters and symbols
\usepackage{wasysym}
\usepackage{marvosym}
%% The lineno packages adds line numbers. Start line numbering with
%% \begin{linenumbers}, end it with \end{linenumbers}. Or switch it on
%% for the whole article with \linenumbers after \end{frontmatter}.
\usepackage{lineno}
% Bash commands
\usepackage{listings}
\usepackage{xcolor}
% COLORED TITLE HEADERS AND SECTIONS
\usepackage{titlesec}
% verbatim text
\usepackage{alltt}

\begin{document}


%
%make title bold and 14 pt font (Latex default is non-bold, 16 pt)
\title{\Large \bf Disfluency detection}

\author{\makebox[.9\textwidth]{\bf Vidya Ayer}\\Faculty of Technology \\ Bielefeld University \thanks{David Schlangen, Faculty of Linguistics, Bielefeld University} \thanks{Julian Hough}}

\date{05-Nov-2018}
\maketitle


%S-1
\section{ABSTRACT} \label{abstract}
%S-1

In this chapter we describe the analysis of the disfluency framework software that was used in the research work on artificial conversational systems, done for the paper \textbf{Joint, Incremental Disfluency Detection and Utterance Segmentation from Speech} for dialogue processing tasks[\citealp{Hough2017}] under the aegis of the \textbf{Digital Signal Processing} linguistics group at Bielefeld University. This library uses a mixed data model of live \textbf{automatic speech recognition (ASR)} data and a cleaned text corpus of open data to demonstrate the deep learning systems for disfluency detection rates in conversational systems and related tasks on speech data.

\newcommand{\keywords}[1]{\textbf{\textit{Keywords:}} #1}
\begin{keywords}
Linguistics, Speech Recognition, Python, Machine learning, LSTM, HMM, RNN, NLTK, Theano, Keras
\end{keywords}


%S-2
\section{INTRODUCTION} \label{intro}
%S-2

The Dialogue Systems Group at Bielefeld University, located at the Faculty of Linguistics and Literary Studies and the \textbf{Cluster of Excellence Cognitive Interaction Technology} (CITEC) studies artificial conversational systems that are a valuable addition to the existing set of psychiatric health care delivery solutions. \textbf{Speech recognition} (SR), also known as \textbf{automatic speech recognition} (ASR), \textbf{computer speech recognition} or \textbf{speech to text} (STT), is a sub-field within computational linguistics that automates the recognition and translation of spoken language into text by machines. 

An artificial system can ensure that interview protocols are followed, and, interactions with such conversational agents have been shown to contain interpretable markers of psychological distress, such as rate of filled pauses, speaking rate, and various temporal, utterance and turn-related interactional features[\citealp{DeVault2013}]. 

Currently, these systems are only used to elicit material that is then analysed offline of transcripts with gold standard utterance segmentation. To enable more cost-effective analysis, however, and possibly even let the interaction script itself be dependent on an analysis hypothesis, their experiment worked directly off the speech signal, and online (incrementally). Their paper presents their evaluation of a model that works with online, incremental speech recognition output to detect disfluencies with various degrees of fine-grainedness.

\subsection{Research Objective} \label{ResearchObjective}

\textbf{Speech recognition} (SR), also known as \textbf{automatic speech recognition} (ASR), \textbf{computer speech recognition} or \textbf{speech to text} (STT), is a sub-field within computational linguistics that develops methodologies and technologies that automates the recognition and translation of spoken language into text by machines. 

The research objective was a joint task of incremental disfluency detection and utterance segmentation for a simple deep learning system that performed the detection on trained models, transcripts and ASR results based on previous high-performance results in research work on disfluency detection on pre-segmented utterances in the Switchboard corpus. Since live approaches use acoustic information, the performance results are not at a comparable level to their transcription-based task analogues, due to the lack of fine-grained analysis of disfluency structure that is used to identify the disfluency type and compute its meaning. 

The recent improved performance, in terms of low Word Error Rate (WER) and better live performance properties on live incremental approaches to both tasks has benefited ASR hypotheses and increased its reliability. Their paper defines a joint task in a live setting and after defining the experiment task, the simple deep learning system which simultaneously detects disfluencies and predicts up-coming utterance boundaries from incremental word hypotheses and derived information is presented. Their research goal was to observe whether the constraints of the two tasks interact, discover if the joint-task system outperforms the equivalent individual task systems and provided competitive results, thereby demonstrating its suitability and potential to be used within conversation agents in the health (psychiatric) domain.

For the joint ASR task they ask the following questions of scalable, automatic approache systems:

\begin{itemize}
\item \textbf{Q1}: Given the interaction between the two tasks, can a system which performs both jointly help improve equivalent systems doing the individual tasks?
\item \textbf{Q2}: Given the incremental availability of word timings from state-of-the-art ASR, to what extent can word timing data help performance of either task?
\item \textbf{Q3}: To what extent is it possible to achieve a good online accuracy vs. final accuracy trade-off in a live, incremental, system?
\end{itemize}

The research experiment used deep learning driven incremental disfluency detection and utterance segmentation which was detectable for the Switchboard (swda) data corpus. Since the live ASR system data used in the original experiment for obtaining the results published in the EACL paper were not available for the Conquaire reproducibility experiment, we worked on testing their off-the-shelf tagger demo program; the disfluency tagging on pre-segmented utterances tags repair structure incrementally as per the demo notebook \textit{demos/demo.ipynb}. The Switchboard data corpus test in the demo uses a joint dependency parsing framework for testing and an analysis of their computational {deep\_disfluency} framework is presented for the git commit hash \textbf{4c57a19}\footnote{\url{<https://github.com/dsg-bielefeld/deep_disfluency/commit/4c57a194433af9601ebef0e4c9a451cce4c06252>}\href{https://github.com/dsg-bielefeld/deep_disfluency/commit/4c57a194433af9601ebef0e4c9a451cce4c06252}{(4c57a19)}} in this chapter.


%S-3
\section{METHODS} \label{methods}
%S-3
Human speech patterns vary between individuals and unlike humans machines have to overcome the obstacle of nuances and diversity in vocal patterns. These obstacles can be mitigated by machine learning algorithms and techniques used in speech recognition training systems. 
Training the automatic speech recognition (ASR) system requires large volumes of high-quality data to ensure that the system can understand human speech in a variety of environments. In order to properly train a speech recognition system and improve its artificial intelligence (AI) quotient, quality information and highly accurate data sets are required for training the model; otherwise, the model will not produce effective results. Here, we describe the methods used for the experimental settings in the deep disfluency library. 

The parts of speech (POS) are tagged for each word incrementally which can be an incremental diff (difference). 


%SS-3.1
\subsection{Experiment settings and Data acquisition pipeline} \label{DataIn}
%SS-3.1

The installation of the \textbf{deep disfluency} library is automated as a \textbf{pip} package with the command:
\lstinline[style=BashInputStyle]$ pip install deep_disfluency$

The computational reproducibility section \ref{ReX} describes the installation process in detail. At the core of a deep learning model  there is a training loop, with multiple iterations, that looks for convergence, then quits. Monitoring progress and measuring accuracy over N iterations, requires taking periodic model snapshots to improve model parameters when the model approaches convergence over the time-series epochs.


%SS-3.2
\subsection{Training Models} \label{TrainModels}
%SS-3.2
The speech models are trained on millions of pre-translated words and phrases from corpora against a live ASR system. For incremental ASR, they use the free trial version of IBM’s \textbf{Watson}\footnote{\url{<https://www.ibm.com/watson/developercloud/speech-to-text.html>}\href{https://www.ibm.com/watson/developercloud/speech-to-text.html}{Watson}} Speech-To-Text (STT) service. 

The deep disfluency system uses the following input features:

\begin{itemize}
\item Words in a backwards window from the most recent word (transcribed or ASR)
\item Durations of words in the current window (from transcription or ASR word timings)
\item Part-Of-Speech (POS) tags for words in current window (either reference, or from an incremental CRF tagger)
\end{itemize}
    
In a training model, if one variation of a word, or parts of speech (POS), appears more than another, the program will favour the more common translation. The internal POS tagger program tags each word incrementally for an incremental diff for the final tags, then the whole tagger is reset for a new utterance. Upon execution, a part of the output on the command line is as follows: 

\begin{verbatim}
\lstinline[style=BashInputStyle]$```
******************************
2. Joint disfluency tagger and utterance semgenter
Simple disf tags <e/>, <i/> and repair onsets <rps
LSTM simple from Hough and Schlangen EACL 2017
Initializing Tagger
Processing args from config number 35 ...
Intializing model from args...
Using the cpu
Warning: not using GPU, might be a bit slow
Adjust Theano config file ($HOME/.theanorc)
loading tag to index maps...
\end{verbatim}


%SS-3.1
\subsubsection{Taggers} \label{taggers}
%SS-3.1

The deep disfluency tagger takes input words (and optionally, POS tags and word timings) word-by-word and outputs xml-style tags for each disfluent word, symbolising each part of any repair or edit term detected. The tags are:

\begin{verbatim}
`<e/>` - an edit term word, not necessarily inside a repair structure

`<rms id=“N”/>` - reparandum start word for repair with ID number N

`<rm id=“N”/>` - mid-reparandum word for repair N

`<i id=“N”/>` - interregnum word for repair N

`<rps id=“N”/>` - repair onset word for repair N (where N is normally the 0-indexed position in the sequence)

`<rp id=“N”/>` - mid-repair word for repair N

`<rpn id=“N”/>` - repair end word for substitution or repetition repair N

`<rpndel id=“N”/>` - repair end word for a delete repair N
\end{verbatim}

Every repair detected or in the gold standard will have at least the \textbf{rms}, \textbf{rps} and \textbf{rpn/rpndel} tags, but the others may not be present.

Some example output on Switchboard utterances is as below, where \textbf{<f/>} is the default tag for a fluent word:

\begin{verbatim}
4617:A:15:h		1	uh          	UH	    <e/>
2	i	        PRP	    <f/>
3	dont	    	VBPRB	    <f/>
4	know	    	VB	    <f/>

4617:A:16:sd		1	the         	DT          <rms id="1"/>
2	the	        DT	    <rps id="1"/><rpn id="1"/>
3	things	    	NNS	    <f/>
4	they	    	PRP	    <f/>
5	asked	    	VBD         <f/>
6	to	        TO	    <f/>
7	talk	    	VB	    <f/>
8	about	    	IN          <f/>
9	were	    	VBD	    <f/>
10	whether	    	IN	    <rms id="12"/>
11	the	        DT	    <rm id="12"/>
12	uh	        UH	    <i id="12"/><e/>
13	whether	    	IN	    <rps id="12"/>
14	the	        DT	    <rpn id="12"/>
15	judge	    	NN	    <f/>
16	should	    	MD	    <f/>
17	be	        VB	    <f/>
18	the	        DT	    <f/>
19	one	        NN	    <f/>
20	that	    	WDT	    <f/>
21	does	    	VBZ	    <f/>
22	the	        DT	    <f/>
23	uh	        UH	    <e/>
24	sentencing	NN	    <f/>
\end{verbatim}

Recurrent neural networks (RNNs) addresses the issue of machines lacking human-like persistent memory. They are networks with loops in them, allowing information to persist.

%SS-3.2.2
\subsubsection{Hidden Markov Models - HMMs} \label{HMM}
%SS-3.2.2

The Deep Disfluency framework depends on libre libraries that implement HMM for speech recognition. Hidden Markov Models (HMM)s are the core of every modern speech recognition system with unique and detailed modeling techniques being developed within this framework over the years. It is a simple way to model sequential data with the implication that in a Markov Model the underlying data is hidden or unknown. Only the observational data and not information about the states is available. 

The disfluency tagger uses Markov Models to train and make an educated guess about the tagger, utterances and POS structure via the token (data) output that is dependent on the state that is visible. Each state has a probability distribution over the possible output tokens creating a pattern of tokens for the sequence of states that can be stored for analysis.

%SS-3.2.3
\subsubsection{Recurrent Neural Networks - RNN} \label{RNN}
%SS-3.2.3

Keras implements the Recurrent Neural Networks (RNN) that is used for  speech disfluency detection and prediction in the package. The recurrent neural network (RNN) is an artificial neural network where connections between nodes form a directed graph for a sequence over epochs. The RNNs use their internal state memory to process a sequences of inputs which is useful for speech recognition tasks such as (un)segmented utterences, etc.. For the models, all RNNs were trained for a maximum of 50 epochs, where an \textbf{epoch} is a moment in time, or timeseries, used as a reference for LSTM processing. 

%SS-3.2.4
\subsubsection{Long short-term Memory (LSTM)} \label{lstm}
%SS-3.2.4

LSTMs are a special kind of recurrent neural network that is used by the disfluency tagger to look at recent information to perform the tagging task for the speech data, swda corpus and voice. For POS-tagging, they use the NLTK CRF tagger, which is trained on the training data and tested for accuracy on all tags till it achieves a good F-score. With long-term dependencies, the advantage of an RNN is the ability to connect previous information to the present task, eg. over an epoch, for a training model with accurate convergence that is then stored in the config folders.

%SS-3.2.5
\subsubsection{Keras} \label{keras}
%SS-3.2.5

Keras\footnote{\url{<https://en.wikipedia.org/wiki/Keras>}\href{https://en.wikipedia.org/wiki/Keras}{Keras}} is a high-level open source neural-network library based on Python that runs on CPU or GPU with support for convolutional and recurrent networks running atop Theano. It is a popular library used in classification, text generation and summarization, tagging, parsing, machine translation, speech recognition, among other uses.


%SS-3.2.6
\subsubsection{Theano} \label{theano}
%SS-3.2.6

Theano\footnote{\url{<https://en.wikipedia.org/wiki/Theano_(software)>}\href{https://en.wikipedia.org/wiki/Theano_(software)}{Theano}} is a numerical computation Python library and optimizing compiler for evaluating matrix-based mathematical expressions that enables users to create their own machine learning models. It was developed by a Montreal Institute for Learning Algorithms at the Université de Montréal but since 2017, further development, support and maintenance has been stopped.

Frameworks like Keras are built on top of Theano, making it a tool that is often used for machine translation, speech recognition, word embedding, and text classification. The setups and application of RNN to speech is not trivial and require a good estimation before training to make the whole system converge. 


%S-4
\section{COMPUTATIONAL REPRODUCIBILITY} \label{ReX}
%S-4

In this section, we describe the implementation of the Deep Disfluency library, a software library that is designed for the speech analysis of voice and textual data corpus. The DSG group were aware of and interested in computational reproducibility for their research publications. They made efforts to sync their research work by using open toolkits and having a public git repository. They have a github organisational account where the software, a Python-based libre software stack, used in their research projects, is stored.

%sss-4.1
\subsection{Deep Disfluency} \label{disfluency}
%sss-4.1
The Deep Disfluency code is packaged and available on a github repository with documentation outlining the data availability and installation process. The user can install it as a python package via pip, the python installer. There are two options, viz., 
\begin{verbatim}
\lstinline[style=BashInputStyle]$ pip install deep\_disfluency$
\end{verbatim}
which pulls and installs the current PyPI (Python Package Index) library version; or, a manual installation using `setuptools`:
\begin{verbatim}
\lstinline[style=BashInputStyle]$ pip install setuptools$
\end{verbatim}

The corpus data download is automated with an installation script that pulls the data and stores it in the users local machine. The structure of the disfluency system, viz. the corpus, data, the tagger and decoder, evaluations, language training models, experiments, etc., are described in the methods section \ref{methods}. 
 

%SS-4.2
\subsection{Computational Cycle} \label{computeCycle}
%SS-4.2

The code can be used to run the experiments on Recurrent Neural Networks (RNNs) and LSTMs after installation. The disfluency model of type LSTM loads the saved weights from the experiments folder for the respective epoch. When a POS tagger is not specified, it loads the default CRF switchboard tagger; and the same is done when no timer is specified. The loaded decoder uses the Markov model for processing the switchboard data. The disfluency annotations and disfluency detection data are stored in the data folder (within subfolders) for the repaired annotations in files. The tagger token can be reset for the next dialogue. 


%SS-4.2.1
\subsubsection{Demos} \label{demos}
%SS-4.2.1

After installation, the demo program, located in the \textit{demo} folder, can be run. This initializes the tagger, for pre-segmented utterances tags repair structure incrementally, from the config file with a config number and save it in the model directory. 

The images (Fig.1 and 2) show the notebook output when the tagger and utterance segmenter repairs onsets for configurations using LSTM, RNNs. 

\begin{figure}[!ht]}
    \centering
    \includegraphics[width=12cm,height=30cm,keepaspectratio]{images/fig1-deepdisfluency-demo-ipynb-scrshot.png}
    \label{Fig.1: Tagger output in the Deep Disfluency demo.ipynb file}
    \caption{Tagger output in the Deep Disfluency demo.ipynb file}
\end{figure}

The error message in the notebook cell in Fig.1 screenshot taken from the github repo is a local sklearn (scikit-learn) dependency problem, not a tagger bug.

The demo code in the Jupyter (ipython) notebook ran the tagger that demonstrated their processing workflow. 


\begin{figure}[!ht]
    \centering
    \includegraphics[width=12cm,height=30cm,keepaspectratio]{images/fig2-localjupyter-demo-ipynb-scrshot.png}
    \label{Fig.2: Tagger output from the local demo.ipynb file}
    \caption{Tagger output from the local demo.ipynb file}
\end{figure}



%SS-4.2.2
\subsubsection{Data Workflow Lifecycle} \label{datLC}
%SS-4.2.2


\begin{figure}[!ht]
    \centering
    \includegraphics[width=12cm,height=30cm,keepaspectratio]{images/fig3c6-workflow.pdf}
    \label{Fig.3: Workflow of the computational model in the Deep Disfluency demo.ipynb file}
    \caption{Workflow of the computational model in the Deep Disfluency demo.ipynb file}
\end{figure}



The data workflow lifecycle outlines the sequence of data processing and tasks for the demo file that shows the the tagger and POS segmenter processing the corpus data for the data pulled by the package. The data consisted of CRF switchboard data using Markov models, RNN and LSTM techniques to parse the words and POS for disfluency tagging. 


\begin{verbatim}
# Initialize the tagger from the config file with a config number
# and saved model directory
MESSAGE = """1. Disfluency tagging on pre-segmented utterances
tags repair structure incrementally and other edit terms <e/>
(Hough and Schlangen Interspeech 2015 with an RNN)
"""
print MESSAGE
disf = DeepDisfluencyTagger(
config_file="experiments/experiment_configs.csv",
config_number=21,
saved_model_dir="experiments/021/epoch_40"
)

# Tag each word incrementally
# Notice the incremental diff
# Set diff_only to False if you want the whole utterance's tag each time
with_pos = False
print "tagging..."
if with_pos:
# if POS is provided use this:
print disf.tag_new_word("john", pos="NNP")
print disf.tag_new_word("likes", pos="VBP")
print disf.tag_new_word("uh", pos="UH")
print disf.tag_new_word("loves", pos="VBP")
print disf.tag_new_word("mary", pos="NNP")
else:
# else the internal POS tagger tags the words incrementally
print disf.tag_new_word("john")
print disf.tag_new_word("likes")
print disf.tag_new_word("uh")
print disf.tag_new_word("loves")
print disf.tag_new_word("mary")
print "final tags:"
for w, t in zip("john likes uh loves mary".split(), disf.output_tags):
print w, "\t", t
disf.reset()  # resets the whole tagger for new utterance

\end{verbatim}


%sss-5.3.1
\subsubsection{Software toolkit and File IO standards} \label{SWfileIO}
%sss-5.3.1
Their research group used a Free and Open Source Software (FOSS) based Python stack consisting of NLP libraries, like NLTK; with machine learning libraries like Theano (now defunct) for deep learning and written to support the Python-2.7 version. 

It uses \textbf{setuptools} to install the dependencies that include the now defunct Theano, a machine learning library by the Montreal Institute for Learning Algorithms (MILA), University of Montreal. In 2017, after the release of ver-1.0, they ended development including no implementation of new features, with minimal maintenance for one year and thereafter, it would shift to low-maintenance mode, i.e. no security bug fixing patches or feature developments would take place. 

The Deep Disfluency package requires more than 85 Python dependencies and some of the important scientific computing libraries (and circular dependencies) are briefly listed below:

\begin{itemize}
\item \textbf{NLTK}: the SWDA CORPUS Readers, etc... 
\item \textbf{gensim}: a libre vector space modeling and topic modeling toolkit 
\item \textbf{h5py}: A python library for the Hierarchical Data Format (HDF) file format. 
\item \textbf{Jupyter-IPython}: packages required include ipykernel, ipython-genutils, ipywidgets, jupyter-core, jupyter-client, jupyter-console, jupyter-core, nbconvert, nbformat, etc. 
\item \textbf{Keras}: a neural network library.
\item \textbf{Cython}: For fast C-like performance.
\item \textbf{matplotlib}: visualisation library
\item \textbf{numpy}: supports large, multi-dimensional arrays and matrices and related high-level mathematical functions.
\item \textbf{pandas}: library for timeseries, data manipulation and analysis.
\item \textbf{scikit-learn}: libre machine learning library
\item \textbf{scipy}: Libre Python library for scientific computing and technical computing - optimization, linear algebra, FFT, etc.
\item \textbf{Theano}: (Defunct) Optimizing compiler for manipulating and evaluating mathematical expressions, especially matrix-valued ones using GPUs.
\end{itemize}


%SS-5.3
\subsubsection{Technical Challenges} \label{techBugs}
%ss-5.3

\paragraph{Obsolete versions and Dependency hell}
Software, whether closed source or libre, needs an active company or community around it inorder to maintain, develop and support it over newer versions of the platform (both hardware and software) dependencies. 

\textbf{Theano}: A key component of this library is based on Theano which is now defunct as it is unsupported since 2017. This is an unknown danger which affects sustainability and makes it costlier to maintain existing software. When choosing library components it is important to consider choosing packages that will be around on a long-term basis and have an active community around them. 

\textbf{Python-2.7}: Similarly, the deep disfluency code is written in Python-2.7 which will have to be ported to 3.x and re-structured to accommodate a replacement for the machine learning library Theano. 
Porting software is not a trivial task in terms of the time and effort required to rewrite features that existed inorder to continue using the software for the existing research workflow.

\paragraph{Original Voice Data}
The primary raw data used in the disfluency research project for ASR live voice recording analysis was unavailable for the Conquaire reproducibility experiment. Since live voice analysis analysis differ between people, it is important to preserve raw data, plan its storage and sustain its availability as a mandatory research artefact as per the FAIR data guidelines. 


%S-5
\section{REVIEW AND RECOMMENDATIONS} \label{RevRec}
%S-5
An artificially intelligent ASR system with a fully-customized AI model would include features like custom annotations, customized meta-tags and verbatim text transcription. The deep disfluency package is a step toward the direction of an interactive health assessment system, eg. conversation agents in the psychiatric domain.

The role of using Free and Open Source Software (FOSS) is significant as it simplifies the software access and ensures reuse for a researcher interested in computational reproducibility of the data. 

Ideally, research data that fullfills the FAIR criteria of being \textit{Findable}, \textit{Accessible}, \textit{Interoperable} and \textit{Re-usable} eases the transborder international data-sharing initiatives. Open Data projects that encourage data-pooling between research projects ensure long-term data reuse and maintenance.

Some simple practices that encourage publishing open research and encourage scientists to practice open notebook science to publish and communicate scientific knowledge are described below: 

% ss-5.1
\subsection{Recommendations for Research Data FAIR-ness} \label{FAIRdat}
% ss-5.1

Scientists can do open research based on the FAIR data principles and it requires following some basic guidelines practices. Data objects can satisfy the FAIR principles by adopting its four facets, viz.:

%SS-5.1.1
\subsubsection{Data should be Findable} \label{fairFind}
%SS-5.1.1
Their research data was a combination of voice data and Open Data sets (eg. switchboard) and since the open data installation process was automated, it reduced the probability of data loss or errors. The software used for the disfluency system was open source and available on github and as a python package via the official Python Package Index (PyPI).

%sss-5.1.2
\subsubsection{Data should be Accessible} \label{fairAccess}
%sss-5.1.2
Currently the original voice data used in the experiments is unavailable. However, from a reproducibility perspective, due to the nuances and diversity of human voices, it is imperitive to have the original voice data set used in the ASR experiments. To enable this data to be findable, the storage and retrieval process for voice data must be automated too. 

\subsubsection{Data should be Interoperable} \label{fairInter}
As per the FAIR guidelines, \textbf{interoperable} data requires machine-readable metadata\footnote{\url{<http://blog.ukdataservice.ac.uk/fair-data-assessment-tool/>}\href{http://blog.ukdataservice.ac.uk/fair-data-assessment-tool/}{metadata}} that describes the digital resource for their research data. Currently, there is no metadata for the ASR codebase but due to the open nature of the library, the github URL to a document that contains machine-readable metadata for the digital resource specifying the file formats would be a first step to create a semantic metadata resource for the research data.

%sss-5.1.4
\subsubsection{Data should be Reusable} \label{fairReuse}
%sss-5.1.4
The data repository was provided openly on github, thus enabling easy access to the repository. More importantly, this allowed multiple collaborators within the subject field to collaborate remotely, hence enabling data reuse and sharing. 



%S-6
\section{CONCLUSION}
%S-6
This chapter has showcased a case study from the speech recognition and computational linguistics field, ran the demo notebooks for the ASR system, machine learning libraries, described the benefits of libre software, open data and python package management.

From the computational reproducibility perspective, this results in good data management practices and is very much aligned with the FAIR data guidelines. 

This paper has described a case study in computational reproducibility in the area of computational linguistics and speech recognition. We have in particular investigated the issue of how disfluency present in speech influences the computational analyses performed on simulation

As suggested in the review and recommendations section \ref{RevRec}, the research project already satisfies many aspects of the FAIR data principles as it adopts open software practices.


\bibliographystyle{plain}
\bibliography{bib7ch}
\end{document}

