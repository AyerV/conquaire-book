\subsection{A brief description of FLAME}

The \textit{Flexible Large-scale Agent Modelling Environment} (FLAME)
is a generic agent-based modelling platform, which can be used to generate
agent-based models in any area of application.\footnote{See the FLAME website \href{http://www.flame.ac.uk}{\url{http://www.flame.ac.uk}} for documentation. The software components XParser and Libmboard can be downloaded from the FLAME GitHub repository: \href{https://github.com/FLAME-HPC/}{\url{https://github.com/FLAME-HPC/}}.}
FLAME models are currently being actively employed in various areas of research such as biological applications, crowd simulations, and economic analyses (see the FLAME website for examples and code).

In principle, FLAME is not a simulator, but a simulation generator since it creates an executable that can be run on any hardware platform from laptops or servers, to HPC clusters. Currently there exist different flavors of FLAME, for use with CPUs or GPUs, and efforts are underway to create a single, uniform environment that addresses all hardware architectures. The CPU version is called FLAME-HPC and is currently the most mature version. For more detailed descriptions of FLAME we refer to \citet{FLAME:2010,FLAME-1:2012,FLAME-CPU:2012,FLAME-GPU:2011,FLAME-2:2016,Kiran:2017}.

Several features make this simulation environment particularly
appealing as a framework for the development and analysis of large-scale agent-based 
models in economics. Since FLAME has been specifically designed for the use on high-performance computing clusters, this
is a big advantage for simulating large-scale agent-based models. It
provides a very transparent and clean way to model information flows between
agents using messages, both internal inside the conceptual model and outside of it through the use of a Message Passing Interface (MPI), provided by the Libmboard library.

The only means to communicate private data between agents is through using such
messages, where the data an agent can transmit consists of a
list of values of its own state variables (e.g.~wealth, income, skills,
profits, expectations about certain variables). Messages are added to a
centralized message board and the sender determines which agents can read the
message. Agents check the message boards in every iteration in order to collect all the
information they are supposed to receive. An agent can use the collected
information as input to its decision rules or as the basis for updating some of its own
state variables.

In FLAME, since high-performance computing clusters are involved (and computational resources are still scarce),
the data generation and data analysis stages are a multi-stage process in which considerations
of computational time and data storage play an important role. These two steps are separated in time, with the data first 
being generated and stored to disk, and afterwards the data is again loaded for analysis.

At the simulation
design stage (before simulations are actually run), the model analyst can
select to output either a complete snapshot of all variables of all
agents (this is very data intensive), or select a subset of agents for
which all variables will be stored. In addition, it is also possible to
select a certain frequency at which the data is outputted, say every $n$ iterates, or to select only a subset of variables (a much less data intensive mode of simulation).

At the simulation stage, FLAME generically outputs data as XML tagged files, which are then transformed to SQLite databases by a Python script.
Until quite recently, the post-processing was done using the R Project
statistics package (\citealp{R_Project}) in which scripts have been developed for post-processing
the data to automatically generate full sets of graphs (timeseries, boxplots,
scatter plots, histograms). The plots are generated at two levels: aggregate
plots for macro data and disaggregated plots for individual agent data.
Finally, the full sets of graphs are available to analyze single runs, batch
runs (multiple runs) and for parameter sensitivity analysis.

The R scripts are now being replaced by Python scripts using Python pandas for the data processing (see the Section on FLAViz below).

\subsection{Data structures used in FLAME}

FLAME uses the XML format for data input and output files. Over the course of designing a simulation model in FLAME, three types of XML files are typically required:

\begin{enumerate}
\item Model XML files. This file follows a DTD (see FLAME User Manual, \citealp[pp.43-44]{FLAME:2010}). It specifies the model's data structures and variable types, with XML tags for the environment, models, agents, messages, ADTs, and time units. The environment-tag contains static constants (model parameters) and file names for the C function files (user-created). The xagent-tag contains memory variables and functions. Messages and ADTs contain attributes, which are the variables contained in these data containers.

\item Data input XML files. This file is an input argument to the simulator executable (see FLAME User Manual, \citealp[pp.30-31]{FLAME:2010}). It contains all initial values for the model constants and agent variables. Usually the input file is called 0.xml, and the default file size is now about 25 MB for our standard economic model.

\item Data output XML files. These are the output files generated by the simulator executable (which itself is generated by FLAME by compiling the user-created and template C code). This type of file only contains the values for all the agents' variables. The environment constants usually are not outputted (except when the output file is a snapshot, see below), since the constants are static, and already contained in the input file.
\end{enumerate}

Below we describe in more detail the third type of XML file, namely the data output XML files.

\subsubsection{Data Output XML Files}

To better understand the structure and data content of the output XML files, a brief discussion about the notion of `agents' might be helpful.  
In our research we deal with different economic `agent types', such as Eurostat, Bank, Firm, Household, Central bank. 
Each agent type has a different set of variables, since this depends on what activities the agent performs in the model. For example, the agent type `Bank' might contain variables such as cash, total credit, deposits, mean interest rate, etc. Another agent of type `Eurostat' might contain variables like: unemployment rate, total debt, monthly output, average wage, etc.

Also, each agent `type' is an archetype, and many `instances' of each agent type may actually exist in the simulation. In this sense, the agent types can be likened to an object class, and the individual agents are similar to object instances.

Depending on the particular type of economic analysis we are interested in, we have different requirements for the simulation output. Therefore, the `agent types' and `variables' can be filtered. 
For example, a particular simulation might contain only the agent type Eurostat, while for another simulation we might need more than one agent type, for example all Eurostat, Firm and Bank agents.

Furthermore, the number of variables can be filtered as per requirements, i.e. for an agent of type `Eurostat' we can store only the variable `monthly output', or we may store more than one variable like `unemployment rate', and `total debt'.  Similarly, variables of the other agent types can be filtered out, on a type-specific basis (it is currently not possible to filter out variables at the individual agent level). This is one reason why the output XML files may vary in size. Some common file sizes are: 105 bytes (store only Eurostat, 1 variable), 2 MB (store multiple agent types, multiple instances of each type, and many variables per agent instance), 25 MB (store a population snapshot, containing all agents, and all variables per agent).

\textbf{Note:} The population snapshot file of 25 MB has a slightly different structure than the other output files because it is a snapshot. In addition to the agent variables this file also contains the model constants/parameters. These static constants are usually not outputted, since they are already contained in the input XML file (0.xml). But since the purpose of the snapshot file is to be used as an input again to the simulator, the model constants must be contained in this file.


\subsubsection{Output XML file naming convention}
The output XML files are named for the iteration numbers. Basically, a file named 1.xml contains all values at the end of iteration 1; similarly 2.xml contains all values  at the end of iteration 2, and so on.

\subsubsection{Data conversion scripts}
In the process of producing the FLAViz library we decided to move away from SQLite database format as storage method for the output data. Also the use of the raw FLAME-generated XML files to store the data is not sustainable in the future. We are therefore moving to a new storage format similar to database files called \href{https://www.hdfgroup.org}{HDF5}. More about the HDF5 format and related issues will be discussed later on, in relation to the data analysis and visualization library.